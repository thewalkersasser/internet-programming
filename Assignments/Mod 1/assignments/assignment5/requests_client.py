import requests
import os

# gather url and filename
weblink = raw_input("Enter website url: ")
filename = raw_input("Enter filename: ")

try:
    # GET request on the weblink that was entered in
    r=requests.get(weblink)

    # if the filename already exist, just open it and overwrite the content
    if os.path.isfile(filename):
        # open file in 'write bytes wb' mode
        with open(filename, 'wb') as f: 
            # write contents of response (r) to file
            f.write(r.content)
    # if filename does not exist  
    else:
        # create the file using w+
        f = open(filename, 'w+')
        f.close() # close file
        # reopen file in write bytes mode
        f = open(filename, 'wb')
        # write content to file
        f.write(r.content)
        # close the file
        f.close

    # print outcome to user
    print("Response from {} written to {} successfully".format(weblink, filename))
except Exception as e:
    # print error if something went wrong
    print("Something went wrong: ", e)
