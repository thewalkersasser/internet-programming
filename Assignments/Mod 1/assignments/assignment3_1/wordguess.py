import random

class wordguesser:
    def __init__(self):
        self.letters = []
        self.guessedLetters = []
        self.gameOver = False

    def generateWord(self):
        nouns = ['people',
                'history',
                'way',
                'art',
                'world',
                'information',
                'map',
                'two',
                'family',
                'government',
                'health',
                'system',
                'computer',
                'meat',
                'year',
                'thanks',
                'music',
                'person',
                'reading',
                'method',]

        index = random.randint(0, len(nouns))

        self.letters = list(nouns[index].upper())

        # add this line if you want to see the word and cheat!
        print("Word: ", self.letters)

        for letter in self.letters:
            self.guessedLetters.append("_")

        print("\nguessedLetters Array: ", self.guessedLetters)
        return

    def guessLetter(self, guessedLetter):

        
        for letter in self.letters:
            if (guessedLetter in self.letters):
                index = self.letters.index(guessedLetter)
                self.guessedLetters[index] = guessedLetter
                print("\nCongrats, you guessed a letter!")
                break

        print("Current Progress: ", self.guessedLetters)

        for i in range(len(self.letters)):
            if (self.letters[i] != self.guessedLetters[i]):
                return
            


        self.gameOver = True
        print("You have guessed the word! Game Over...")
        return


wordguesser = wordguesser()
wordguesser.generateWord()
while (not wordguesser.gameOver):
    guessedLetter = input("Enter a letter: ").upper()
    wordguesser.guessLetter(guessedLetter)