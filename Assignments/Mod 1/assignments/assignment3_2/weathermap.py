import requests
import datetime

api_key = raw_input('Enter api key: ')

zip_code = input('Enter zip code: ')

url = "https://api.openweathermap.org/data/2.5/weather?zip={},{}&appid={}".format(zip_code,'US', api_key)

response = requests.get(url)

data = response.json()

#if there was no response
if data["cod"] != "404": 
    
    x = data["main"] 
    
    print("\nData for {}:".format(zip_code))

    temperature = x["temp"] 
    print("\nTemperature: {} F".format((temperature - 273.15) * (9/5) + 32))

    pressure = x["pressure"] 
    print("\nPressure: {} hPa".format(pressure))

    wind = data["wind"]['speed']
    print("\nWind Speed: {} mph".format(wind))

    wind_direction = data["wind"]['deg']
    print("\nWind Direction: {} degrees".format(wind_direction))

    z = data["weather"] 

    desc = z[0]["description"] 
    print("\nDescription: {}".format(desc))

    time = datetime.datetime.now()
    print("\nTimestamp: {}".format(time))
  
else: 
    print(" Zip Code Not Found ") 




