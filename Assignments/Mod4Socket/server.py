import socket
import logging
import polynomials

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((socket.gethostname(), 1235))
s.listen(10) 

while True:
    clientsocket = s.accept()
    sock = clientsocket[0]
    
    request = ""
    message = sock.recv(1024)
    while len(message) > 0:
        request+=message.decode()
        message = sock.recv(1024)
    
    if len(request) == 0:
        message = 'Error: Empty Request'
        sock.sendall(message.encode())
        sock.shutdown(1)
    else:
        req_code = request[0]
        
        coefficients_temp = request[1:].split(' ') # remove spaces
        coefficients = []
        if len(coefficients_temp) < 2:
            message = "XToo few args: must be at least 2"
            sock.sendall(message.encode())
            sock.shutdown(1)
        elif req_code == 'E':
            for coef in coefficients_temp:
                try:
                    coefficients.append(float(coef)) # convert strings to floats
                except Exception as error:
                    message = "XInproper format for coefficients: " + request[1:]
                    sock.sendall(message.encode())
                    sock.shutdown(1)
            value = coefficients[0] # value to be evaulauted
            if (len(coefficients_temp) >= 2):

                result = polynomials.evaluate(value, coefficients[1:])

                message = "E" + str(result)
                sock.sendall(message.encode())
                sock.shutdown(1)
            
                
            else:
                message = "XToo few args: must be at least 2"
                sock.sendall(message.encode())
                sock.shutdown(1)
           
        elif req_code == 'S':

            for coef in coefficients_temp:
                try:
                    coefficients.append(float(coef)) # convert strings to floats
                except Exception as error:
                    message = "XInproper format for coefficients: " + request[1:]
                    sock.sendall(message.encode())
                    sock.shutdown(1)

            if (len(coefficients_temp) >= 2):

                x = coefficients[0]
            
                y = coefficients[1]
                poly = coefficients[2:len(coefficients)-1]
                tolerance = coefficients[len(coefficients) -1]
                if tolerance <= 0:
                    message = "XInvalid Tolerance: " + str(tolerance)
                    sock.sendall(message.encode())
                    sock.shutdown(1)
                else:
                    result = polynomials.bisection(x, y, poly, tolerance)

                    message = "S" + str(result)
                    sock.sendall(message.encode())
                    sock.shutdown(1)
            
                
            else:
                message = "XToo few args: must be at least 2"
                sock.sendall(message.encode())
                sock.shutdown(1)
        else:
            message = "XInproper request code: " + req_code
            sock.sendall(message.encode())
            sock.shutdown(1)
    sock.close()

