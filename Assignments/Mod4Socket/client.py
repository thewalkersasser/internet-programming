import socket



def send(message):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((socket.gethostname(), 1235))
    message = message.encode()
    s.sendall(message)
    s.shutdown(1)

    response = ""
    message = s.recv(1024)
    while len(message) > 0:
        response += message.decode()
        message = s.recv(1024)

    

    s.close()
    return response

testing_strings = [
"E1.0 -945 1689 -950 230 -25 1",
"S0 2 -945 1689 -950 230 -25 1 1e-15", "G4.1 0 0",
"4 1 0",
"E1.0",
"S1.0",
"Not a number",
"S0 2 -945 1689 -950 230 -25 1 0",
"S0 2 -945 1689 -950 230 G 1 1e-15"]

for test in testing_strings:
    print("Sending Message: " + test + "\n")
    response = send(test)
    print("Received Message: " + response + "\n")
    
    

